import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:comesseq/app/modules/auth/auth_controller.dart';
class AuthPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthController>(
      builder: (_) {
        final _formKey = GlobalKey<FormState>();
        TextStyle style = TextStyle(fontFamily: 'Century Gothic', fontSize: 20.0);
        final scaffoldKey = new GlobalKey<ScaffoldState>();

        return Scaffold(
          key: scaffoldKey,
          body: Center(
          // body: GestureDetector(
            // onTap: () => FocusScope.of(context).unfocus(),
            // child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric( horizontal: 10.0, vertical: 10.0 ),
                child: Material(
                  elevation: 5,
                  color: Colors.white,
                  clipBehavior: Clip.antiAlias,
                  borderRadius: BorderRadius.circular(8),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 20.0,
                        ),
                        Text(
                          "Iniciar sesión",
                          textAlign: TextAlign.center,
                          style: style.copyWith(
                            color: Color(0xffBA181B),
                            fontWeight: FontWeight.bold
                          )
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        inputGenerico(
                          textFormField: inputUser( style, _ )
                        ),
                        inputGenerico(
                          textFormField: inputPassword( style, _ )
                        ),
                        botonGenerico( style, _ )
                      ],
                    ),
                  ),
                )
              ),
            ),
          // ),
        );
      },
    );
  }

  Widget inputGenerico( { @required Widget textFormField } ) {
    return Padding(
      padding: EdgeInsets.all(8),
      child: Material(
        elevation: 0,
        color: Colors.white,
        clipBehavior: Clip.antiAlias,
        borderRadius: BorderRadius.circular(8),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Container(
              width: 320.0,
              padding: EdgeInsets.only(
                left: 20,
                right: 50,
                top: 0
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Colors.grey[200]
              ) ,
              child: textFormField
            ),
          ]
        )
      )
    );
  }

  Widget inputUser( TextStyle style, AuthController _ ) {
    return TextFormField(
      onChanged: _.onUsernameChanged,
      style: style,
      obscureText: false,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: '*Usuario',
        icon: Icon(Icons.person),
        hintText: 'Cédula',
      ),
      validator: (String value) {
        if ( value.isEmpty ) {
          return 'Ingrese su documento';
        }
        return null;
      },
      // keyboardAppearance: TextInputType.number,
    );
  }

  Widget inputPassword( TextStyle style, AuthController _ ) {
    return Obx(() {
      return TextFormField(
        onChanged: _.onPasswordChanged,
        style: style,
        obscureText: !_.shoPassword.value,
        decoration: InputDecoration(
          labelText: '*Contraseña',
          icon: Icon( Icons.lock_open ),
          hintText: 'Contraseña',
          suffixIcon: IconButton(
            onPressed: () {
              bool mostrar = _.shoPassword.value;
              _.onShowPassword( !mostrar );
            },
            icon: _.shoPassword.value ? Icon(Icons.visibility) : Icon(Icons.visibility_off),
          ),
        ),
        keyboardType: TextInputType.number,
        validator: (String value) {
          if ( value.isEmpty ) {
            return 'Ingrese su contraseña';
          }
          return null;
        },
      );
    });
  }

  Widget botonGenerico( TextStyle style, AuthController _ ) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Material(
        elevation: 0,
        color: Color(0xffBA181B),
        clipBehavior: Clip.antiAlias,
        borderRadius: BorderRadius.circular(8),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            MaterialButton(
              padding: EdgeInsets.symmetric(horizontal: 50),
              onPressed: _.submit,
              child: Text(
                "Ingresar",
                textAlign: TextAlign.center,
                style: style.copyWith(
                  color: Colors.white,
                  fontWeight: FontWeight.bold
                )
              ),
            ),
          ]
        )
      )
    );
  }
}