import 'package:comesseq/app/data/providers/comesseq_api.dart';
import 'package:meta/meta.dart' show required;

class HomeRepository {

  final ComesseqApiClient apiClient;

  HomeRepository({ @required this.apiClient}) : assert(apiClient != null);

  getAll(){
    // return apiClient.getAll();
  }
  getId(id){
    // return apiClient.getId(id);
  }
  delete(id){
    // return apiClient.delete(id);
  }
  edit(obj){
    // return apiClient.edit( obj );
  }
  add(obj){
    // return apiClient.add( obj );
  }

}