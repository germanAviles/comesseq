
import 'package:comesseq/app/core/values/constants.dart';
import 'package:comesseq/app/data/providers/auth_api.dart';
import 'package:comesseq/app/data/providers/storage_provider.dart';
import 'package:comesseq/app/data/repositories/storage_repository.dart';
import 'package:comesseq/app/modules/auth/auth_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';

class DependencyInjection{

  static void init() {
    Get.lazyPut(
      () => Dio( BaseOptions( baseUrl: Constants.BASE_API ))
    );
    Get.lazyPut(() => AuthProvider() );
    Get.lazyPut(() => AuthRepository() );
    Get.lazyPut(() => FlutterSecureStorage() );
    Get.lazyPut(() => StorageProvider() );
    Get.lazyPut(() => StorageRepository() );
  }
}