
import 'package:comesseq/app/data/models/loguedUser.dart';
import 'package:comesseq/app/data/providers/storage_provider.dart';
import 'package:get/get.dart';

class StorageRepository {

  final StorageProvider _storageProvider = Get.find<StorageProvider>();

  // saveUsuario: Guardamos el usuario logueado
  void saveUserAuth({ Map<String, dynamic> usuario }) => _storageProvider.saveUserAuth(usuario);

  // getAuthUser: Obtenemos el usuario logueado
  Future<UserAuthenticated> getAuthUser() => _storageProvider.getUserAuth();

}