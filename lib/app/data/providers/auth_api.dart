// import 'package:dio/dio.dart';
import 'package:comesseq/app/core/values/constants.dart';
import 'package:comesseq/app/data/models/loguedUser.dart';
import 'package:meta/meta.dart' show required;
import 'package:http/http.dart' as http;
import 'dart:convert';
class AuthProvider {
  // final Dio _dio = Get.find<Dio>();

  Future<UserAuthenticated> validateWithLogin({
    @required String username,
    @required String password,
  }) async {
    print("================ provider ================");
    // final dynamic response = await _dio.post(
    //   '/api-login.php',
    //   data: {
    //     'user': username,
    //     'password': password,
    //     'token': Constants.TOKEN_LOGIN
    //   },
    // );
    final url = Uri.https(Constants.API_DATASSEQ, '/APIs/api-login.php', {'q': '{https}'});

    final dynamic response = await http.post(
      url,
      body: {
        'token': Constants.TOKEN_LOGIN,
        'user': username,
        'password':password
      }
    );

    final resParsed = jsonDecode(response.body);
    final String userString = resParsed[0];
    final List<String> userArray = userString.split('-');

    Map<String, dynamic> usuario = Map();
    usuario['cedula'] = userArray[0];
    usuario['nombre'] = userArray[1];
    usuario['token'] = null;

    print('[USUARIO] $usuario');
    return UserAuthenticated.fromJson( usuario );
  }

}
