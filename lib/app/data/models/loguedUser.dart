import 'package:meta/meta.dart' show required;
class UserAuthenticated {

  final String nombre;
  final String cedula;
  final String token;

  UserAuthenticated({
    @required this.nombre,
    @required this.cedula,
    @required this.token,
  });


  factory UserAuthenticated.fromJson(Map<String, dynamic> json) => UserAuthenticated(
    nombre: json['nombre'],
    cedula: json['cedula'],
    token: json['token']
  );

  // Map<String, dynamic> toJson(){
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['name'] = this.name;
  //   return data;
  // }

  Map<String, dynamic> toJson() => {
    "nombre": nombre,
    "cedula": cedula,
    "token": token,
  };
}