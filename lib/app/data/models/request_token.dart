import 'package:meta/meta.dart' show required;
class RequestToken {

  final bool success;
  final String expiresAt;
  final String requestToken;

  RequestToken({
    @required this.success,
    @required this.expiresAt,
    @required this.requestToken
  });


  factory RequestToken.fromJson(Map<String, dynamic> json) => RequestToken(
    success: json['success'],
    expiresAt: json['expiresAt'],
    requestToken: json['requestToken']
  );

  // Map<String, dynamic> toJson(){
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['name'] = this.name;
  //   return data;
  // }

  Map<String, dynamic> toJson() => {
    "success": success,
    "expiresAt": expiresAt,
    "requestToken": requestToken
  };
}