import 'package:get/get.dart';

import 'package:comesseq/app/modules/auth/auth_binding.dart';
import 'package:comesseq/app/modules/auth/auth_page.dart';
import 'package:comesseq/app/modules/home/home_binding.dart';
import 'package:comesseq/app/modules/home/home_page.dart';
part './routes.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
      name: Routes.HOME,
      page: () => HomePage(),
      binding: HomeBinding()
    ),
    GetPage(
      name: Routes.AUTH,
      page: () => AuthPage(),
      binding: AuthBinding()
    ),
    // GetPage(name: Routes.DETAILS, page:()=> DetailsPage(), binding: DetailsBinding()),
  ];
}
